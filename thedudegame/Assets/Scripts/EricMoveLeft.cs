﻿using UnityEngine;
using System.Collections;

public class EricMoveLeft : MonoBehaviour {

	public float SpeedX = 100;
	public float pauseTime = 0;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		rigidbody2D.AddForce (new Vector2 (-SpeedX, 0));
		CheckOutOfBound ();
		if (!audio.isPlaying && audio.enabled) {
			audio.Play ();
		}
	}

	void CheckOutOfBound()
	{
		if (transform.position.x<-15) {
			gameObject.audio.Stop();
			Destroy(this.gameObject);
		}
	}
}
