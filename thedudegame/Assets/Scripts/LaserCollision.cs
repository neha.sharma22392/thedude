﻿using UnityEngine;
using System.Collections;

public class LaserCollision : MonoBehaviour {

	public GameObject particleSystemTemplate;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnTriggerEnter2D( Collider2D item ) {

		ParticleSystem particleObject = Instantiate(particleSystemTemplate, item.transform.position, item.transform.rotation) as ParticleSystem;

		if(particleObject !=null)
		{
			//Destroy Particle effect once its done.
			Destroy(particleObject, particleObject.particleSystem.duration);
		}

		//Destroy ghost
		Destroy(item.gameObject);
	}
}
