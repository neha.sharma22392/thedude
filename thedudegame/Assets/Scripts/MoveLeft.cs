﻿using UnityEngine;
using System.Collections;

public class MoveLeft : MonoBehaviour {

	public float Speed = 2;

	// Use this for initialization
	void Start () {
		rigidbody2D.velocity = new Vector2(-Speed, 0);
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x<-50) {
			Destroy(this.gameObject);
		}
	}
}
