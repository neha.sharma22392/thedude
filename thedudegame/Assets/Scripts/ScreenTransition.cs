﻿using UnityEngine;
using System.Collections;

public class ScreenTransition : MonoBehaviour {

	public string loadLevel;
	public float LoadTimer = 1.5f;
	private float _loadTimerTicker = 0;

	private Color _originalColor;

	// Use this for initialization
	void Start () {
		guiTexture.pixelInset = new Rect (0f, 0f, Screen.width, Screen.height);
		_originalColor = guiTexture.color;
	}

	void Update()
	{
		_loadTimerTicker += Time.deltaTime;

		if(_loadTimerTicker> LoadTimer )
		{
			if(Input.GetButtonDown("Fire1") || Input.GetKeyDown("space"))
		    {
				Application.LoadLevel(loadLevel);
			}
		}

		float t = _loadTimerTicker / LoadTimer;
		if(t >1)
		{
			t = 1;
		}

		guiTexture.color = Color.Lerp(Color.clear, _originalColor, t);
	}
}
